
import { AntdSkDialog }  from '../../sk-dialog-antd/src/antd-sk-dialog.js';

export class Antd4SkDialog extends AntdSkDialog {

    get prefix() {
        return 'antd4';
    }

}
